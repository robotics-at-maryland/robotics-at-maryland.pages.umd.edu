"use client";

import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import * as React from "react";
import SEO from "@/components/SEO";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";

import Image from "@/components/Image";
import { Typography } from "@mui/material";

const QuboTechnicalImage = {
  src: "/qubo-cad-7.1.24.png",
  alt: "Qubo Technical Image",
};

const QuboTechnicalImage2 = {
  src: "/qubo-technical-image-2.png",
  alt: "Qubo Technical Image",
};

const MechanicalOverviewImage = {
  src: "/qubo_by_pool.jpg",
  alt: "Qubo Technical Image",
};

const TorpedoLauncherSystemImage = {
  src: "/qubo_torpedo.png",
  alt: "Torpedo Launcher System",
};

const ElectronicsHullImage = {
  src: "/electronics-hull.png",
  alt: "Electronics Hull",
};

const PneumaticsHydraulicsSystemImage = {
  src: "/pneumatics-hydraulics-system.png",
  alt: "Pneumatics Hydraulics System",
};

const ClawImage = {
  src: "/claw.png",
  alt: "Claw",
};

const BackplaneImage = {
  src: "/qubo_backplane.jpg",
  alt: "Qubo Backplane",
};

const PowerDistributionBoard = {
  src: "/power-distribution-board.png",
  alt: "Power Distribution Board",
};

const SensorSuite = {
  src: "/sensor-suite.png",
  alt: "Sensor Suite and Mako Visual Camera",
};

const ElectricalEngineeringPhoto = {
  src: "/electrical-engineering.png",
  alt: "Electrical Engineering Workbench",
};

const ComputerVisionImage = {
  src: "/computer-vision.png",
  alt: "Computer Vision",
};

const EmbeddedImage = {
  src: "/embedded.png",
  alt: "Embedded Systems",
};

const ComputerVision2Image = {
  src: "/computer-vision-2.png",
  alt: "Stereo Camera Output",
};

const SoftwareOverviewImage = {
  src: "/software-overview.png",
  alt: "Software Overview",
};

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography component="div">{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function Qubo() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <SEO />
      <Grid container>
        <Grid item sm={12} lg={4} maxHeight="calc(100vh - 60px)">
          <Box
            backgroundColor="#EDEDED"
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: { lg: "right", xs: "center" },
              alignItems: "center",
              height: "calc(100vh - 60px)",
            }}
          >
            <Box
              maxWidth="80%"
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: { lg: "left", xs: "center" },
              }}
            >
              <Typography
                sx={{ textAlign: { lg: "left", xs: "center" } }}
                fontFamily="Oswald"
                color="#E21833"
                fontSize="150px"
                lineHeight="140px"
              >
                Dive into Qubo
              </Typography>

              <Typography
                sx={{ textAlign: { lg: "left", xs: "center" } }}
                paddingTop="50px"
                fontSize="30px"
                fontWeight="bold"
                color="primary"
              >
                noun. [pron. que-BO] The University of Maryland’s second
                generation competitive AUV.
              </Typography>
            </Box>
          </Box>
        </Grid>

        <Grid item sm={12} lg={8}>
          <Box
            sx={{
              display: "flex",
              paddingTop: "20px",
              backgroundColor: "#EDEDED",
              height: { xs: "auto", lg: "calc(100vh - 60px)" },
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Box
              flexGrow="1"
              sx={{
                maxHeight: "100%",
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <img
                style={{ width: "100%", maxHeight: "100%", objectFit: "cover" }}
                src={QuboTechnicalImage2.src}
                alt={QuboTechnicalImage2.alt}
              />
            </Box>
          </Box>
        </Grid>
      </Grid>

      {/* <Grid container>
                <Grid item lg = {6}>

                </Grid>
                <Grid lg = {6}>

                </Grid>
            </Grid> */}

      <Grid
        container
        justifyContent="center"
        style={{ backgroundColor: "#000000" }}
      >
        <Grid item lg={9}>
          <Typography
            paddingTop="40px"
            paddingBottom="20px"
            color="white"
            fontSize="40px"
            fontWeight="bold"
          >
            Components + Parts
          </Typography>
        </Grid>
      </Grid>

      <Grid
        sx={{ display: { lg: "flex", xs: "none" } }}
        paddingBottom="40px"
        container
        columnSpacing={5}
        justifyContent="center"
        style={{ backgroundColor: "#000000" }}
      >
        <Grid
          item
          md={3}
          sx={{ display: "flex", flexDirection: "column", padding: "10px" }}
        >
          <Typography color="white" fontWeight="bold" fontSize="21px">
            Dimensions
          </Typography>
          <Typography color="white" fontSize="17px">
            0.4 x 0.3 x 0.3 meters
          </Typography>
          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Structural Components
          </Typography>
          <Typography color="white" fontSize="17px">
            Blue Robotics 6&quot; Watertight Enclosure
          </Typography>
          <Typography color="white" fontSize="17px">
            Purple Buoyancy Foam (From SSL Stock)
          </Typography>
          <Typography color="white" fontSize="17px">
            Custom Made Frame
          </Typography>
          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Sonar Equipment
          </Typography>
          <Typography color="white" fontSize="17px">
            PCB Piezotronics 106B50 Acoustic Pressure Sensor
          </Typography>
          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Mass
          </Typography>
          <Typography color="white" fontSize="17px">
            49.5 pounds, 22.452 kgs
          </Typography>
          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Maximum Velocity
          </Typography>
          <Typography color="white" fontSize="17px">
            Untested
          </Typography>
        </Grid>

        <Grid item md={3}>
          <Typography color="white" fontWeight="bold" fontSize="21px">
            Propulsion Control
          </Typography>
          <Typography color="white" fontSize="17px">
            Blue Robotics T200 Thrusters + Propellors (x8)
          </Typography>
          <Typography color="white" fontSize="17px">
            Blue Robotics Basic ESC
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            High - Level Control
          </Typography>
          <Typography color="white" fontSize="17px">
            Teledyne Explorer Depth Velocity Logger (DVL)
          </Typography>
          <Typography color="white" fontSize="17px">
            PNI TRAX1 Attitude Heading Reference System (AHRS)
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Batteries
          </Typography>
          <Typography color="white" fontSize="17px">
            Gens Ace 5000mAh 4S Lithium Polymer Battery
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Connectors
          </Typography>
          <Typography color="white" fontSize="17px">
            Blue Robotics M10 Potted Connectors
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Custom-Made Components
          </Typography>
          <Typography color="white" fontSize="17px">
            Torpedo Launch System 
          </Typography>
        </Grid>

        <Grid item md={3}>
          <Typography color="white" fontWeight="bold" fontSize="21px">
            On Board Software
          </Typography>
          <Typography color="white" fontSize="17px">
            Robot Operating System v. 8 (Humble)
          </Typography>
          <Typography color="white" fontSize="17px">
            Ubuntu 22.04
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Development and Simulation Software
          </Typography>
          <Typography color="white" fontSize="17px">
            Gazebo Sim 11.0.0
          </Typography>
          <Typography color="white" fontSize="17px">
            SolidWorks 2023
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Computers and Boards
          </Typography>
          <Typography color="white" fontSize="17px">
            Seeed Studio reComputer J2021 - Edge{" "}
          </Typography>
          <Typography color="white" fontSize="17px">
            with Jetson Xavier NX8 GB
          </Typography>
          <Typography color="white" fontSize="17px">
            Custom Power Board (with Voltage Stepper)
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Stereo Vision Cameras
          </Typography>
          <Typography color="white" fontSize="17px">
            Stereolabs ZED 2 AI Stereo Camera
          </Typography>
          <Typography color="white" fontSize="17px">
            Allied Vision Mako G - 131C 1280 x 1024 GigE Camera
          </Typography>
        </Grid>
      </Grid>

      <Grid
        sx={{ display: { lg: "none", md: "flex", xs: "none" } }}
        paddingBottom="40px"
        container
        columnSpacing={5}
        justifyContent="center"
        style={{ backgroundColor: "#000000" }}
      >
        <Grid
          item
          md={5}
          sx={{ display: "flex", flexDirection: "column", padding: "10px" }}
        >
          <Typography color="white" fontWeight="bold" fontSize="21px">
            Dimensions
          </Typography>
          <Typography color="white" fontSize="17px">
            0.4 x 0.3 x 0.3 meters
          </Typography>
          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Structural Components
          </Typography>
          <Typography color="white" fontSize="17px">
            Blue Robotics 6&quot; Watertight Enclosure
          </Typography>
          <Typography color="white" fontSize="17px">
            Purple Buoyancy Foam (From SSL Stock)
          </Typography>
          <Typography color="white" fontSize="17px">
            Custom Made Frame
          </Typography>
          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Sonar Equipment
          </Typography>
          <Typography color="white" fontSize="17px">
            PCB Piezotronics 106B50 Acoustic Pressure Sensor
          </Typography>
          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Mass
          </Typography>
          <Typography color="white" fontSize="17px">
            49.5 pounds, 22.452 kgs
          </Typography>
          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Maximum Velocity
          </Typography>
          <Typography color="white" fontSize="17px">
            Untested
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Propulsion Control
          </Typography>
          <Typography color="white" fontSize="17px">
            Blue Robotics T200 Thrusters + Propellors (x8)
          </Typography>
          <Typography color="white" fontSize="17px">
            Blue Robotics Basic ESC
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            High - Level Control
          </Typography>
          <Typography color="white" fontSize="17px">
            Teledyne Explorer Depth Velocity Logger (DVL)
          </Typography>
          <Typography color="white" fontSize="17px">
            PNI TRAX1 Attitude Heading Reference System (AHRS)
          </Typography>
        </Grid>

        <Grid item md={5}>
          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Batteries
          </Typography>
          <Typography color="white" fontSize="17px">
            Gens Ace 5000mAh 4S Lithium Polymer Battery
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Connectors
          </Typography>
          <Typography color="white" fontSize="17px">
            Blue Robotics M10 Potted Connectors
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Custom-Made Components
          </Typography>
          <Typography color="white" fontSize="17px">
            Torpedo Launch System 
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            On Board Software
          </Typography>
          <Typography color="white" fontSize="17px">
            Robot Operating System v. 8 (Humble)
          </Typography>
          <Typography color="white" fontSize="17px">
            Ubuntu 22.04
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Development and Simulation Software
          </Typography>
          <Typography color="white" fontSize="17px">
            Gazebo Sim 11.0.0
          </Typography>
          <Typography color="white" fontSize="17px">
            SolidWorks 2023
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Computers and Boards
          </Typography>
          <Typography color="white" fontSize="17px">
            Seeed Studio reComputer J2021 - Edge{" "}
          </Typography>
          <Typography color="white" fontSize="17px">
            with Jetson Xavier NX8 GB
          </Typography>
          <Typography color="white" fontSize="17px">
            Custom Power Board (with Voltage Stepper)
          </Typography>

          <Typography
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Stereo Vision Cameras
          </Typography>
          <Typography color="white" fontSize="17px">
            Stereolabs ZED 2 AI Stereo Camera
          </Typography>
          <Typography color="white" fontSize="17px">
            Allied Vision Mako G - 131C 1280 x 1024 GigE Camera
          </Typography>
        </Grid>
      </Grid>

      <Grid
        sx={{ display: { md: "none", xs: "flex" } }}
        paddingBottom="40px"
        container
        columnSpacing={5}
        justifyContent="center"
        style={{ backgroundColor: "#000000" }}
      >
        <Grid
          item
          xs={10}
          sx={{ display: "flex", flexDirection: "column", padding: "10px" }}
        >
          <Typography
            align="center"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Dimensions
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            0.4 x 0.3 x 0.3 meters
          </Typography>
          <Typography
            align="center"
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Structural Components
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Blue Robotics 6&quot; Watertight Enclosure
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Purple Buoyancy Foam (From SSL Stock)
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Custom Made Frame
          </Typography>
          <Typography
            align="center"
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Sonar Equipment
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            PCB Piezotronics 106B50 Acoustic Pressure Sensor
          </Typography>
          <Typography
            align="center"
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Mass
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            49.5 pounds, 22.452 kgs
          </Typography>
          <Typography
            align="center"
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Maximum Velocity
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Untested
          </Typography>

          <Typography
            align="center"
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Propulsion Control
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Blue Robotics T200 Thrusters + Propellors (x8)
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Blue Robotics Basic ESC
          </Typography>

          <Typography
            align="center"
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            High - Level Control
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Teledyne Explorer Depth Velocity Logger (DVL)
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            PNI TRAX1 Attitude Heading Reference System (AHRS)
          </Typography>

          <Typography
            align="center"
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Batteries
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Gens Ace 5000mAh 4S Lithium Polymer Battery
          </Typography>

          <Typography
            align="center"
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Connectors
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Blue Robotics M10 Potted Connectors
          </Typography>

          <Typography
            align="center"
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Custom-Made Components
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Torpedo Launch System 
          </Typography>

          <Typography
            align="center"
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            On Board Software
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Robot Operating System v. 8 (Humble)
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Ubuntu 22.04
          </Typography>

          <Typography
            align="center"
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Development and Simulation Software
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Gazebo Sim 11.0.0
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            SolidWorks 2023
          </Typography>

          <Typography
            align="center"
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Computers and Boards
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Seeed Studio reComputer J2021 - Edge{" "}
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            with Jetson Xavier NX8 GB
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Custom Power Board (with Voltage Stepper)
          </Typography>

          <Typography
            align="center"
            paddingTop="20px"
            color="white"
            fontWeight="bold"
            fontSize="21px"
          >
            Stereo Vision Cameras
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Stereolabs ZED 2 AI Stereo Camera
          </Typography>
          <Typography align="center" color="white" fontSize="17px">
            Allied Vision Mako G - 131C 1280 x 1024 GigE Camera
          </Typography>
        </Grid>
      </Grid>

      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        paddingBottom="40px"
        paddingTop="40px"
      >
        <Box sx={{ width: { lg: "80%" } }}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="basic tabs example"
            >
              <Tab label="Mechanical" {...a11yProps(0)} />
              <Tab label="Electrical" {...a11yProps(1)} />
              <Tab label="Software" {...a11yProps(2)} />
            </Tabs>
          </Box>
          <TabPanel value={value} index={0}>
            <Grid container spacing={10}>
              <Grid item md={6} xs={12} order={{ lg: 1, xs: 1 }}>
                <Typography
                  fontSize="40px"
                  fontWeight="bold"
                  lineHeight={1.25}
                  paddingBottom={3}
                >
                  Mechanical Engineering Overview
                </Typography>
                <Typography>
                  The mechanical engineering team has been working hard this
                  year to bring important advances to Qubo. Some of the key
                  improvements our team have worked on this year are a
                  modernized and brand new torpedo launcher system, an improved
                  electronics hull, initial development on a pneumatics and
                  hydraulics system that will be continued next semester, and a
                  claw system with a unique design focused on simplicity and
                  effectiveness. These new updates have helped replace old
                  mechanisms, leading to an overall enhancement of Qubo&apos;s
                  capabilities.{" "}
                </Typography>
              </Grid>

              <Grid
                item
                sm={9}
                md={6}
                xs={12}
                order={{ lg: 2, xs: 2 }}
                margin="auto"
              >
                <img
                  backgroundSize="contain"
                  width="100%"
                  src={MechanicalOverviewImage.src}
                  alt={MechanicalOverviewImage.alt}
                />
              </Grid>

              <Grid
                item
                sm={9}
                md={6}
                xs={12}
                order={{ md: 3, xs: 4 }}
                margin="auto"
              >
                <img
                  width="100%"
                  src={TorpedoLauncherSystemImage.src}
                  alt={TorpedoLauncherSystemImage.alt}
                />
              </Grid>

              <Grid item md={6} xs={12} order={{ md: 4, xs: 3 }}>
                <Typography
                  fontSize="40px"
                  fontWeight="bold"
                  lineHeight={1.25}
                  paddingBottom={3}
                >
                  Torpedo Launcher System
                </Typography>
                <Typography>
                  The self-propelled torpedo project underwent a complete
                  redesign from the previous year, replacing springs with
                  supercapacitors and a motor. Its primary objective is to
                  remain securely held by the robot until released, propelling
                  itself forward in a straight line. This precision addresses
                  the unreliable nature of traditional spring-powered mechanisms
                  directly. The entire torpedo project comprises only five
                  components: barrels that provide grooves for the torpedo and
                  housings for the levers, two levers equipped with magnets to
                  activate the torpedo and prevent it from dislodging, and two
                  piston mounts that connect and actuate the levers. A standout
                  feature of this project is its electrical circuitry, visible
                  through clear resin, which adds to its uniqueness and
                  functionality.{" "}
                </Typography>
              </Grid>

              <Grid item md={6} xs={12} order={{ md: 5, xs: 5 }}>
                <Typography
                  fontSize="40px"
                  fontWeight="bold"
                  lineHeight={1.25}
                  paddingBottom={3}
                >
                  Electronics Hull
                </Typography>
                <Typography>
                  Qubo has taken a modular approach to its main hull, through
                  its consistent mounting holes and cube-like shape (which was
                  inspiration for our name, Cube-O!). It is made of 6061
                  aluminum, which is relatively inexpensive and extremely
                  resistant to water corrosion. The main panels were cut from a
                  water jet, which uses a high power stream of water to cut
                  cleanly through the material. From here, all the components
                  can be attached with separate brackets - this strategy allows
                  the robot to be easily modified as the team adapts to new
                  technologies and makes upgrades. Of these components are 8
                  Blue Robotics T200 Thrusters, which allow for precise movement
                  and control in pitch roll and yaw. There’s also room for a
                  massive Doppler Velocity Logger (DVL), multiple cameras, a
                  torpedo launcher and a claw. Qubo contains all of its
                  electronics in a battery hull and a main electronics hull.
                  This hull was developed by Blue Robotics, but filled with our
                  own custom PCBs, computers and drivers to power our systems
                  and control the robot.{" "}
                </Typography>
              </Grid>

              <Grid
                item
                sm={9}
                md={6}
                xs={12}
                order={{ md: 6, xs: 6 }}
                margin="auto"
              >
                <img
                  width="100%"
                  src={ElectronicsHullImage.src}
                  alt={ElectronicsHullImage.alt}
                />
              </Grid>

              <Grid
                item
                sm={9}
                md={6}
                xs={12}
                order={{ md: 7, xs: 8 }}
                margin="auto"
              >
                <img
                  width="100%"
                  src={PneumaticsHydraulicsSystemImage.src}
                  alt={PneumaticsHydraulicsSystemImage.alt}
                />
              </Grid>

              <Grid item md={6} xs={12} order={{ md: 8, xs: 7 }}>
                <Typography
                  fontSize="40px"
                  fontWeight="bold"
                  lineHeight={1.25}
                  paddingBottom={3}
                >
                  Pneumatics and Hydraulics System
                </Typography>
                <Typography>
                  The largest mechanical development on Qubo has been the
                  implementation of a custom pneumatics system. Compressed air
                  is regulated from a paintball tank into an enclosure
                  containing 12V solenoid valves. Four of these valves direct
                  air to pistons that produce actuations for Qubo&apos;s primary
                  end effectors. Two pistons are used to fire two torpedoes, one
                  to actuate the marker dropper, and one to actuate the claw.
                  Altogether, this system creates a much more reliable method of
                  actuation compared to servos. Last year, the team faced many
                  challenges when integrating end effectors. Unreliable servos
                  leaking water into our hull led to no end effectors working
                  during competition, and a rotational motion is much less
                  valuable than linear motion, with the team often using
                  complicated mechanisms to produce an actuation that is easily
                  achieved with pistons. With much more space available from a
                  new DVL, it was ideal to put a pneumatic system in its place,
                  built from scratch by the team to learn the engineering
                  process.
                </Typography>
              </Grid>

              <Grid item md={6} xs={12} order={{ md: 9, xs: 9 }}>
                <Typography
                  fontSize="40px"
                  fontWeight="bold"
                  lineHeight={1.25}
                  paddingBottom={3}
                >
                  Claw System
                </Typography>
                <Typography>
                  In order to interact with markers and bins during competition,
                  we developed a compact claw mechanism. The claw employs a
                  pneumatic piston and a rack and pinion system for opening and
                  closing. Opening the claw involves the piston extending
                  forward, which moves the rack forward, thereby rotating the
                  pinions and opening the claws. Closing the claw reverses this
                  process as the piston retracts. The gripper&apos;s role is to
                  securely hold objects grabbed by the claw, achieved using
                  3D-printed TPU material. The first layer of the gripper
                  includes a groove that fits into the claw mechanism, with
                  screws fastening the gripper and claw together. The second
                  layer features a hexagonal pattern designed to compress under
                  pressure, ensuring a firm grip. The final layer has a simple
                  groove pattern to enhance gripping capability. For mounting
                  and attachment, two brackets securely encase the claw and
                  piston. These brackets are fastened together with screws that
                  thread into heat-set inserts. Hooks on the brackets grip
                  Qubo&apos;s lower bars to securely attach the claw mechanism.{" "}
                </Typography>
              </Grid>

              <Grid
                item
                sm={9}
                md={6}
                xs={12}
                order={{ md: 10, xs: 10 }}
                margin="auto"
              >
                <img width="100%" src={ClawImage.src} alt={ClawImage.alt} />
              </Grid>
            </Grid>
          </TabPanel>
          <TabPanel value={value} index={1}>
            <Grid container spacing={10}>
              <Grid item md={6} xs={12} order={{ lg: 1, xs: 1 }}>
                <Typography
                  fontSize="40px"
                  fontWeight="bold"
                  lineHeight={1.25}
                  paddingBottom={3}
                >
                  Electrical Engineering Overview
                </Typography>
                <Typography>
                  {" "}
                  Last year, Qubo&apos;s electrical infrastructure faced
                  significant challenges, including mechanical failures from
                  shipping, fragile assembly processes, and system-wide failures
                  due to a monolithic power/control board. Additionally,
                  transient shorts on the 12V power bus caused damage despite
                  protective measures. This year, we aim to solve these issues
                  with a robust design strategy featuring a backplane and
                  daughter card system, a custom STM32 PCB, and dedicated ESC
                  and power cards, all of which enhance reliability, modularity,
                  and protection.{" "}
                </Typography>
              </Grid>

              <Grid
                item
                sm={9}
                md={6}
                xs={12}
                order={{ lg: 2, xs: 2 }}
                margin="auto"
              >
                <img
                  backgroundSize="contain"
                  width="100%"
                  src={ElectricalEngineeringPhoto.src}
                  alt={ElectricalEngineeringPhoto.alt}
                />
              </Grid>

              <Grid
                item
                sm={9}
                md={6}
                xs={12}
                order={{ md: 3, xs: 4 }}
                margin="auto"
              >
                <img
                  width="100%"
                  src={BackplaneImage.src}
                  alt={BackplaneImage.alt}
                />
              </Grid>

              <Grid item md={6} xs={12} order={{ md: 4, xs: 3 }}>
                <Typography
                  fontSize="40px"
                  fontWeight="bold"
                  lineHeight={1.25}
                  paddingBottom={3}
                >
                  Custom Backplane System
                </Typography>
                <Typography>
                  Our new backplane system is a 4-layer PCB designed to host
                  multiple cards using 96-position Eurocard connectors. This
                  system significantly reduces wire bulk inside the hull by 90%,
                  providing a cleaner and more organized setup. The Eurocard
                  connectors were selected for their versatility and robustness,
                  enabling reliable power and signal conduction at a low cost.
                  The modular nature of the backplane allows for easy swapping
                  of cards in case of failure, ensuring minimal downtime and
                  streamlined maintenance.
                </Typography>
              </Grid>

              <Grid item md={6} xs={12} order={{ md: 5, xs: 5 }}>
                <Typography
                  fontSize="40px"
                  fontWeight="bold"
                  lineHeight={1.25}
                  paddingBottom={3}
                >
                  Sensor Suite (DVL, Zed Camera, Mako, IMU)
                </Typography>
                <Typography>
                  The Zed2i stereo camera is mounted forward-facing as the main
                  camera. The Mako camera is mounted downward facing. The
                  teledyne explorer DVL is used as a velocity sensor. The
                  Doppler velocity logger (DVL) sends out hypersonic waves, then
                  measures depth and velocity by how long the waves take to get
                  back and through understanding of the Doppler effect.{" "}
                </Typography>
              </Grid>

              <Grid
                item
                sm={9}
                md={6}
                xs={12}
                order={{ md: 6, xs: 6 }}
                margin="auto"
              >
                <img
                  width="100%"
                  src={ElectronicsHullImage.src}
                  alt={ElectronicsHullImage.alt}
                />
              </Grid>

              <Grid
                item
                sm={9}
                md={6}
                xs={12}
                order={{ md: 7, xs: 8 }}
                margin="auto"
              >
                <img
                  width="100%"
                  src={PowerDistributionBoard.src}
                  alt={PowerDistributionBoard.alt}
                />
              </Grid>

              <Grid item md={6} xs={12} order={{ md: 8, xs: 7 }}>
                <Typography
                  fontSize="40px"
                  fontWeight="bold"
                  lineHeight={1.25}
                  paddingBottom={3}
                >
                  Power Distribution Board
                </Typography>
                <Typography>
                  The ESC cards are designed to mount four Electronic Speed
                  Controllers (ESCs) directly onto the daughter card,
                  eliminating the majority of the wire bulk associated with ESC
                  wiring in Qubo. By integrating ESCs onto dedicated cards, we
                  enhance the modularity and maintainability of the system,
                  reducing the risk of wiring failures and simplifying the
                  overall electrical layout. The new power card design improves
                  upon the current power and control board by incorporating
                  better circuit protection and isolated WIR DC/DC converters
                  with integrated heat sinks. We are currently testing our
                  assumptions about the eFuses using a development board, with
                  promising results. This power card provides controlled circuit
                  protection against transient events, ensuring a more reliable
                  power distribution system and mitigating the risk of damage
                  from power shorts. By isolating power components and improving
                  heat management, we aim to enhance the overall durability and
                  performance of Qubo&apos;s electrical system.
                </Typography>
              </Grid>
            </Grid>
          </TabPanel>
          <TabPanel value={value} index={2}>
            <Grid container spacing={10}>
              <Grid item md={6} xs={12} order={{ lg: 1, xs: 1 }}>
                <Typography
                  fontSize="40px"
                  fontWeight="bold"
                  lineHeight={1.25}
                  paddingBottom={3}
                >
                  Computer Science and Software Overview
                </Typography>
                <Typography>
                  Our computer science and software team has focused on
                  developing our machine learning algorithms. From being able to
                  better detect gates and objects underwater, to creating much
                  new training data for use in the upcoming school year, our
                  team has been arming Qubo with the best in computer vision
                  advancements. We have also been working hard on improving our
                  documentation and file organization in order to make
                  onboarding easier for our new members. This includes creating
                  future onboarding tasks and developing simulations to help
                  introduce new members to our development process.{" "}
                </Typography>
              </Grid>

              <Grid
                item
                sm={9}
                md={6}
                xs={12}
                order={{ lg: 2, xs: 2 }}
                margin="auto"
              >
                <img
                  backgroundSize="contain"
                  width="100%"
                  src={SoftwareOverviewImage.src}
                  alt={SoftwareOverviewImage.alt}
                />
              </Grid>

              <Grid
                item
                sm={9}
                md={6}
                xs={12}
                order={{ md: 3, xs: 4 }}
                margin="auto"
              >
                <img
                  width="100%"
                  src={EmbeddedImage.src}
                  alt={EmbeddedImage.alt}
                />
              </Grid>

              <Grid item md={6} xs={12} order={{ md: 4, xs: 3 }}>
                <Typography
                  fontSize="40px"
                  fontWeight="bold"
                  lineHeight={1.25}
                  paddingBottom={3}
                >
                  Embedded Systems Overview
                </Typography>
                <Typography>
                  Qubo’s embedded systems send control instructions to and
                  collect information from peripherals. They are primarily
                  written in C++ or C. They control the thrusters, the claw,
                  torpedo launchers, and more. Collected information includes
                  orientation, depth, electric current draw, battery status,
                  etc. Peripherals use a variety of communication protocols,
                  including UART, I2C, and Ethernet. Some subsystems need to
                  operate at high speeds and reliable latencies. For example,
                  generating PWM (pulse-width modulation) at reliable speeds
                  requires specialized hardware, so we use a PCA9685 that we
                  control via I2C. Parts of the sonar algorithm need to run at
                  high speeds, so we are designing a custom circuit that will
                  run on a Xilinx Zync UltraScale+ MPSoC FPGA
                  (field-programmable gate array).
                </Typography>
              </Grid>

              <Grid item md={6} xs={12} order={{ md: 5, xs: 5 }}>
                <Typography
                  fontSize="40px"
                  fontWeight="bold"
                  lineHeight={1.25}
                  paddingBottom={3}
                >
                  Localization Technology and Method
                </Typography>
                <Typography>
                  Localization is Qubo’s process of finding out where it is in
                  relation to its environment. This involves combining input
                  from a variety of sensors, handling build up of error
                  (discrepancy between reality and Qubo’s data), and SLAM
                  (simultaneous localization and mapping) techniques. The ZED 2i
                  stereo camera is a big help here as it provides a lot of
                  useful data and has built-in support for SLAM and loop
                  closure.{" "}
                </Typography>
              </Grid>

              <Grid
                item
                sm={9}
                md={6}
                xs={12}
                order={{ md: 6, xs: 6 }}
                margin="auto"
              >
                <img
                  width="100%"
                  src={ComputerVision2Image.src}
                  alt={ComputerVision2Image.alt}
                />
              </Grid>

              <Grid
                item
                sm={9}
                md={6}
                xs={12}
                order={{ md: 7, xs: 8 }}
                margin="auto"
              >
                <img
                  width="100%"
                  src={ComputerVisionImage.src}
                  alt={ComputerVisionImage.alt}
                />
              </Grid>

              <Grid item md={6} xs={12} order={{ md: 8, xs: 7 }}>
                <Typography
                  fontSize="40px"
                  fontWeight="bold"
                  lineHeight={1.25}
                  paddingBottom={3}
                >
                  Computer Vision and Machine Learning
                </Typography>
                <Typography>
                  The purpose of the Computer Vision subteam is to extract
                  actionable information from the imaging data from Qubo’s
                  various cameras. Over the past year, we have been designing
                  algorithms for gate and pipe detection that work with our ROS
                  control system for the Robosub Pre-Qual task in our Gazebo
                  simulation. We developed a gate and pipe detector using the
                  OpenCV library and depth information from the Zed 2i camera.
                  This is used to navigate the robot through the gate, around
                  the pipe, and back through the gate. We also developed a deep
                  learning approach using the YOLOv5 real-time object detection
                  model, which we custom trained on a human-annotated gate
                  dataset and implemented into our ROS system.
                </Typography>
              </Grid>
            </Grid>
          </TabPanel>
        </Box>
      </Box>
    </>
  );
}
