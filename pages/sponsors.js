"use client";
import * as React from "react";
import { Container, Typography, Grid, Button } from "@mui/material";
import Email from "@mui/icons-material/Email";
import SEO from "@/components/SEO";

const Aerospace = {
  src: "/sponsor_aerospace.png",
  alt: "Department of Aerospace Engineering Logo",
};
const Clark = {
  src: "/sponsor_ajc.png",
  alt: "A. James Clark School of Engineering Logo",
};
const Coderpie = {
  src: "/sponsor_coderpie.png",
  alt: "Coderpie Logo",
};
const ComputerScience = {
  src: "/sponsor_cs.png",
  alt: "Department of Computer Science Logo",
};
const CompE = {
  src: "/sponsor_ece.png",
  alt: "Department of Electrical and Computer Engineering Logo",
};
const Mechanical = {
  src: "/sponsor_mechanical.png",
  alt: "Department of Mechanical Engineering Logo",
};
const RoboticsCenter = {
  src: "/sponsor_mrc.png",
  alt: "Maryland Robotics Center Logo",
};
const RoboticResearch = {
  src: "/sponsor_robotics_research.png",
  alt: "Robotics Research Logo",
};
const SpaceSystems = {
  src: "/sponsor_ssl.png",
  alt: "Space Systems Lab Logo",
};
const TerrapinWorks = {
  src: "/sponsor_terrapin_works.png",
  alt: "Terrapin Works Logo",
};

export default function Sponsors() {
  return (
    <>
      <SEO />
      <Container
        maxWidth="false"
        sx={{ py: 15, backgroundColor: "common.white" }}
      >
        <Typography
          variant="h1"
          textAlign="center"
          color="common.black"
          fontWeight={600}
        >
          Become a Sponsor Today
        </Typography>

        <Grid
          container
          spacing={3}
          alignItems="center"
          textAlign="center"
          sx={{ pt: 5 }}
          color="inherit"
        >
          <Grid item sm={2}></Grid>

          <Grid item xs={12} sm={4} justifyContent="center">
            <Button
              href="/ram_sponsor_packet_2023_2024.pdf"
              download="ram_sponsor_packet"
              variant="contained"
            >
              <Typography variant="h6"> Sponsor Packet</Typography>
            </Button>
          </Grid>
          <Grid item xs={12} sm={4} justifyContent="center">
            <Button
              href="mailto:team@ram.umd.edu"
              startIcon={<Email />}
              variant="contained"
            >
              <Typography variant="h6">Contact Us</Typography>
            </Button>
          </Grid>

          <Grid item sm={2}></Grid>

          <Grid item md={2}></Grid>
          <Grid item xs={12} md={8} alignItems="center">
            <Typography paragraph variant="h5" color="grey.800" sx={{ p: 3 }}>
              {" "}
              We are always welcome to new sponsors with contributions of any
              size or form. By becoming a sponsor you will become our mentor
              too! We host events throughout the semester to meet with corporate
              partners and discuss our designs. Collect our resumes, meet the
              team, and get your name front and center on UMD’s largest and most
              vibrant robotics club!{" "}
            </Typography>
          </Grid>
          <Grid item md={2}></Grid>

          <Grid item md={2}></Grid>
          <Grid item xs={12} md={8} alignItems="center">
            <Typography paragraph color="grey.800">
              {" "}
              Gifts in support of the University of Maryland are accepted and
              managed by the University of Maryland College Park Foundation,
              Inc. an affiliated 501(c)(3) organization authorized by the Board
              of Regents. Contributions to the University of Maryland are tax
              deductible as allowed by law. Please see your tax advisor for more
              details.{" "}
            </Typography>
          </Grid>
          <Grid item md={2}></Grid>
        </Grid>
      </Container>

      <Container
        maxWidth="false"
        sx={{ py: 10, backgroundColor: "primary.main" }}
      >
        <Typography
          variant="h2"
          textAlign="center"
          fontWeight={600}
          sx={{ py: 3 }}
        >
          {" "}
          Thank You To Our Sponsors
        </Typography>
      </Container>

      {/* <Container */}
      {/*   maxWidth="false" */}
      {/*   sx={{ py: 10, backgroundColor: "common.white" }} */}
      {/* > */}
      <div className="bg-white">
        <Grid item xs={12} md={4}>
          <Typography
            paragraph
            variant="h6"
            textAlign="left"
            color="common.black"
            sx={{ p: 5 }}
          >
            {" "}
            Robotics at Maryland’s success is solely reliant on the invaluable
            contributions of our generous sponsors. We would like to thank all
            of them for allowing us to not only to prepare our members for a
            career in robotics, but also to foster a love of collaboration,
            challenge, and learning.
          </Typography>
        </Grid>
        <div className="flex w-full justify-center">
          <div className="flex w-[80%] flex-1 flex-row flex-wrap justify-center gap-24 bg-white px-4 pb-16 md:px-32">
            <div className="flex flex-1 basis-64 flex-col items-center gap-8">
              <img src={Coderpie.src} alt={Coderpie.alt} width="65%" />
              <img src={ComputerScience.src} alt={ComputerScience.alt} />
              <img src={RoboticsCenter.src} alt={RoboticsCenter.alt} />
            </div>
            <div className="flex flex-1 basis-64 flex-col justify-around gap-8">
              <img src={RoboticResearch.src} alt={RoboticResearch.alt} />
              <img src={SpaceSystems.src} alt={SpaceSystems.alt} />
              <img src={TerrapinWorks.src} alt={TerrapinWorks.alt} />
            </div>
            <div className="flex flex-1 basis-64 flex-col items-center gap-8">
              <img src={Clark.src} alt={Clark.alt} />
              <div className="flex flex-1 flex-col gap-8">
                <img src={Aerospace.src} alt={Aerospace.alt} />
                <img src={CompE.src} alt={CompE.alt} />
                <img src={Mechanical.src} alt={Mechanical.alt} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
