IMAGE = ram/website

.PHONY: container
container: Dockerfile
	docker build --tag=$(IMAGE) ./

.PHONY: serve
serve: container
	docker run --name ram-website --rm -p 3000:3000 --interactive --tty --mount type=bind,source="$(shell pwd)",target=/website $(IMAGE) yarn dev

.PHONY: build
build: container
	docker run --name ram-website --rm -p 3000:3000 --interactive --tty --mount type=bind,source="$(shell pwd)",target=/website $(IMAGE) yarn build

.PHONY: shell
shell: container
	docker run --name ram-website --rm -p 3000:3000 --interactive --tty --mount type=bind,source="$(shell pwd)",target=/website $(IMAGE) /bin/bash
