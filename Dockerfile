FROM node:lts

WORKDIR /website

COPY package.json yarn.lock ./

RUN yarn install && yarn next telemetry disable

# copy twice to avoid rebuilding container when website code changes
COPY . .

EXPOSE 3000
